#include "Server.h"
#include "Channel.h"
#include "command.h"
#include "string"
#include "ServerDB.h"
#include "execinfo.h"
#include <iterator>
#include "signal.h"
#include "Meta.h"
#include "ACL.h"
#include <QtGui/qtextdocument.h>
#include <vector>
#include <map>
#include <QHostAddress>

using namespace std;
using namespace Abex;

static bool channelSort(const ::Channel *a, const ::Channel *b) {
  return ::Channel::lessThan(a, b);
}

QHostAddress* localip;

bool Abex::showInList(ServerUser *user) {
  QList<QHostAddress> boxAddrs = QNetworkInterface::allAddresses();
  return !boxAddrs.contains(user->haAddress.toAddress());
}

bool Abex::userShouldSeeChannel(ServerUser *user, Channel *channel)
{
  ChanACL::ACLCache cache;
  return ChanACL::hasPermission(user,channel,ChanACL::Enter,&cache);
}

MumbleProto::TextMessage newTextMessage(QString msg)
{
    MumbleProto::TextMessage m;
    m.set_message(u8(msg));
    return m;
}

ServerUser* getUser(Server *server, QString arg)
{
  bool isInt;
  int id = arg.toInt(&isInt);
  if(isInt)
  {
    QHash<unsigned int, ServerUser *>::iterator it = server->qhUsers.find(id);
    if(it==server->qhUsers.end())
      return NULL;
    return it.value();
  }
  else
  {
    for(QHash<unsigned int, ServerUser *>::iterator it = server->qhUsers.begin();it != server->qhUsers.end();it++)
    {
        if(it.value()->qsName==arg)
        return it.value();
    }
    return NULL;
  }
}

struct PrefixProbe {
  QStringRef message;
};
bool operator<(PrefixProbe p, QString s) {
  return p.message < s;
}
bool operator<(QString s, PrefixProbe p) {
  return s < p.message && QStringRef(&s, 0, p.message.size()) == p.message;
}

map<QString, QString, std::less<> > emotes;
QString emoteSearch(QString msg, bool *changed)
{
  *changed = false;
  QString out;
  int lastCopy = 0;
  for(int i=0; i<msg.length(); i++)
  {
    if (msg[i] == '<') {
      // We can't handle HTML, so give up if we see any
      // It likes to corrupt base64 data with small emotes
      break;
    }

    QStringRef needle(&msg, i, msg.length() - i);
    auto upper = emotes.upper_bound(PrefixProbe{needle});
    if (upper == emotes.begin()) {
      continue;
    }
    --upper;

    QString nearestEmote = upper->first;
    if (i + nearestEmote.length() > msg.length()
      || msg.mid(i, nearestEmote.length()) != nearestEmote) {
      continue;
    }
    
    out.append(msg.mid(lastCopy, i - lastCopy));
    i += nearestEmote.length();
    lastCopy = i;
    *changed = true;
    out.append(upper->second);
  }
  if (!*changed) {
    return msg;
  }
  out.append(msg.mid(lastCopy));
  return out;
}


void readEmoteFile(QFile &file, map<QString, QString, std::less<> > &newEmotes, bool insert, QString &keyFind, QString &newValue) {
  if (!file.open(QIODevice::ReadWrite)) {
    qWarning() << "Can't open emote file " << file.fileName();
    return;
  }
  QString all;
  QTextStream fi(&file);
  fi.setCodec(QTextCodec::codecForName("UTF-8"));
  all = fi.readAll(); //sorry but QTextStream is can't not spend all of its time reallocing with big `readLine`s
  fi.seek(0);
  fi.flush();
  int lastWrite = 0;
  int lastRead = 0;
  auto write = [&](size_t to) {
    fi << all.midRef(lastWrite, to-lastWrite);
    lastWrite = to;
  };
  for (;lastRead>=0;) {
    int start = lastRead;
    int tab = all.indexOf('\t', lastRead);
    if (tab < 0) break;
    int newline = lastRead = all.indexOf('\n', tab);
    if (newline < 0) {
      newline = all.size();
    } else {
      lastRead++;
    }
    QString key = all.mid(start, tab-start);
    QString value = all.mid(tab+1, newline-(tab+1));
    if (key.isEmpty()) continue;
    if (key == keyFind) {
      write(start);
      lastWrite = lastRead;
      if (!newValue.isEmpty()) {
        fi << keyFind << "\t" << newValue << "\n";
        newEmotes[keyFind] = newValue;
        newValue = "";
      }
    } else {
      newEmotes[key] = value;
    }
  }
  if (lastWrite || insert) {
    write(all.size());
    if (insert && !newValue.isEmpty()) {
      fi << "\n" << keyFind << "\t" << newValue;
      newEmotes[keyFind] = newValue;
    }
    fi.flush();
    file.resize(file.pos());
  }
}
void readEmotes(QString keyFind = "", QString newValue = "") {
  map<QString, QString, std::less<> > newEmotes;
  QFileInfo dbName(ServerDB::db->databaseName());
  QDir emoteDir(dbName.path() + "/emotes/");
  if(!emoteDir.exists()) {
    if (!emoteDir.mkpath(".")) {
      qFatal("Cannot make emote directory");
    }
  }
  
  QString mumbleName = "mumble";
  foreach (QString name, emoteDir.entryList(QDir::Files | QDir::NoDotAndDotDot)) {
    if (name == mumbleName || name == "dupeCache") {
      continue;
    }

    QFile fi(emoteDir.path() + "/" + name);
    readEmoteFile(fi, newEmotes, false, keyFind, newValue);
  }
  {
    QFile fi(emoteDir.path() + "/" + mumbleName);
    readEmoteFile(fi, newEmotes, true, keyFind, newValue);
  }
  emotes.swap(newEmotes);
}

//QString emoteSearchemoteSearch<map<QString,QString>>(map<QString,QString>, QString, bool*);

void setUserName(Server *server,ServerUser* pUser,QString name)
{
  MumbleProto::UserState mpus;
  mpus.set_name(u8(name));
  mpus.set_session(pUser->uiSession);
  pUser->qsName=name;
  server->sendAll(mpus, ~ 0x010202);
  server->sendAll(mpus, 0x010202);
  //emit userStateChanged(pUser);
}

QString version;
QNetworkAccessManager *netman;
map< int , map< int , QString > > originalNames;
map< int , map< int , bool > > doEcho;
map< int , map< int , QString > > doEnroll;
map<int , int> userFlags;

namespace flags{
  const int ADMIN=0x01;
  const int TRUSTED=0x02;
}

void stacktrace(int sig);

bool isTrusted(int userId)
{
  if(userId==0) return true;
  if(userFlags.find(userId)==userFlags.end()) return false;
  return userFlags[userId]&flags::TRUSTED || userFlags[userId]&flags::ADMIN;
}
bool isAdmin(int userId)
{
  if(userId==0) return true;
  if(userFlags.find(userId)==userFlags.end()) return false;
  return userFlags[userId]&flags::ADMIN;
}

QSqlDatabase AbexDB;

void encode(QString& data) {
    QString buffer;
    buffer.reserve(data.size());
    for(int pos = 0; pos != data.size(); ++pos) {
        switch(data[pos].toLatin1()) {
            case '&':  buffer.append("&amp;");       break;
            case '\"': buffer.append("&quot;");      break;
            case '\'': buffer.append("&apos;");      break;
            case '<':  buffer.append("&lt;");        break;
            case '>':  buffer.append("&gt;");        break;
            default:   buffer.append(data[pos]); break;
        }
    }
    data.swap(buffer);
}
bool cmp(QString needle,QChar thread,QString &data,int &pos,QString &buffer)
{
  if(data.size()-pos<needle.size()) return true;
  bool t =  true;
  for(int i=0;i<needle.size();i++)
  {
    if(data[i+pos]!=needle[i]){
      t=false;
      break;
    }
  }
  //bool t = data.compare(p,c.size(),c)==0;
  if(t)
  {
    pos+=needle.size()-1;
    buffer.insert(buffer.size(),thread);
    return false;
  }
  return true;
}
bool k(...){return true;}
void decode(QString& data)
{
  QString buffer="";
  buffer.reserve(data.size());
  for(int pos = 0; pos < data.size(); ++pos) {
    if(data[pos]=='&')
    {
      cmp("&amp;",'&',data,pos,buffer)&&
      cmp("&quot;",'\"',data,pos,buffer)&&
      cmp("&apos;",'\'',data,pos,buffer)&&
      cmp("&lt;",'<',data,pos,buffer)&&
      cmp("&gt;",'>',data,pos,buffer)&&
      k(&buffer.insert(buffer.size(),data[pos]));
    }
    else
      buffer.insert(buffer.size(),data[pos]);
  }
  data.swap(buffer);
}

#define PERM_ADMIN {if(!isAdmin(uSource->iId))return server->sendTextMessage(NULL,uSource,false,"Permission Denied");}
#define PERM_TRUSTED {if(!isTrusted(uSource->iId))return server->sendTextMessage(NULL,uSource,false,"Permission Denied");}

namespace command {
  void id(Server *server, ServerUser *uSource, QStringList command, QString fullCommand)
  { // /id [name|id|all]
    if(command.length()==2&&command[1]=="all"){
      QString str="<table>";
      foreach(ServerUser *s,server->qhUsers){
        QString name = s->qsName;
        encode(name);
        str+=(QString("<tr><td>%1</td><td>%2</td></tr>").arg(name).arg(s->iId));
      }
      str+="</table>";
      server->sendTextMessage(NULL,uSource,false,str);
      return;
    }
    ServerUser* user;
    if(command.size()>1)
      user = getUser(server,command[1]);
    else
      user = uSource;
    if(user==NULL)
    {
        server->sendTextMessage(NULL,uSource,false,"Cannot find user!");
    }
    else
      server->sendTextMessage(NULL,uSource,false,QString("%1's user ID is %2.").arg(user->qsName).arg(user->iId));
  }
  void name(Server *server, ServerUser *uSource, QStringList command, QString fullCommand)
  {// /name ['new name'|'reset'] [name|id]
    PERM_TRUSTED
    if(command.size()<2) return server->sendTextMessage(NULL,uSource,false,"Not enough parameters!");
    ServerUser *user = uSource;
    if(command.length()==3)
    {
      if(isAdmin(uSource->iId))
        user = getUser(server,command[2]);
      else
        return server->sendTextMessage(NULL,uSource,false,"Permission Denied");
     }
    if(user==NULL) return server->sendTextMessage(NULL,uSource,false,"Cannot find user!");
    QString name = command[1];
    if(name=="reset")
      name = originalNames[server->iServerNum][user->iId];
    setUserName(server,user,name);
  }
  void echo(Server *server, ServerUser *uSource, QStringList command, QString fullCommand)
  {
    if(doEcho[server->iServerNum].count(uSource->iId))
      doEcho[server->iServerNum][uSource->iId] = !doEcho[server->iServerNum][uSource->iId];
    else
      doEcho[server->iServerNum][uSource->iId] = true;
    server->sendTextMessage(NULL,uSource,false,QString("Echoing is %1!").arg(doEcho[server->iServerNum][uSource->iId]?"Enabled":"Disabled"));
  }
  void emote(Server *server, ServerUser *uSource, QStringList command, QString fullCommand)
  {
    if(command.length()==2 && command[1] == "reload") {
      PERM_ADMIN
      readEmotes();
      server->sendTextMessage(NULL, uSource, false, QString("Emotes reloaded; There are now %1 emotes.").arg(emotes.size()));
      return;
    }
    if (command.length() <= 2 || (command.length() == 3 && command[1] == "list")) {
      QString needle;
      switch (command.length()) {
        case 2:
          needle = command[1];
          break;
        case 3:
          needle = command[2];
          break;
        default:
          needle = "";
          break;
      }
      QString needleNext = needle + (QChar)(0xFFFF);
      QString str="";
      for(auto it = emotes.lower_bound(needle); it != emotes.lower_bound(needleNext); ++it) {
        str+=QString("%1<br>").arg(it->first);
      }
      server->sendTextMessage(NULL,uSource,false,str);
      return;
    }
    if(command.length()==3 && command[1] == "add") {
      PERM_ADMIN
      QString needle = command[2];
      doEnroll[server->iServerNum][uSource->iId]=needle;
      server->sendTextMessage(NULL,uSource,false,QString("Next message will be set as '%1'").arg(needle));
      return;
    }
    if(command.length()==3 && command[1] == "clear") {
      PERM_ADMIN
      QString key = command[2];
      readEmotes(key, "");
      server->sendTextMessage(NULL,uSource,false,QString("'%1' is now cleared").arg(key));
      return;
    }
    return server->sendTextMessage(NULL,uSource,false,QString("Unknown command argument %1").arg(command[1]));
  }
  void perm(Server *server, ServerUser *uSource, QStringList command, QString fullCommand)
  { //perm [user]   ->   [0|1|2]
    //perm [user] [0|1|2]  - 1 is admin  2 is trusted
    PERM_TRUSTED
    if(command.length()<2) return server->sendTextMessage(NULL,uSource,false,QString("Not enough Params"));
    if(command.length()==2) // get
    {
      ServerUser *target = getUser(server,command[1]);
      if(target==NULL) return server->sendTextMessage(NULL,uSource,false,QString("Bad user!"));
      QString level = isAdmin(target->iId)?"Admin(1)":(isTrusted(target->iId)?"Trusted(2)":"Pleb-Teir(0)");
      server->sendTextMessage(NULL,uSource,false,QString("User %1 is %2").arg(target->qsName).arg(level));
      return;
    }
    if(command.length()==3)// set
    {
      PERM_ADMIN
      ServerUser *target = getUser(server,command[1]);
      if(target==NULL) return server->sendTextMessage(NULL,uSource,false,QString("Bad user!"));
      int level = command[2].toInt();
      if(level>3 || level<0) return server->sendTextMessage(NULL,uSource,false,QString("Bad level!"));
      int userLevel = 0;
      if(userFlags.find(target->iId)!=userFlags.end())userLevel = userFlags[target->iId];
      userLevel&=~0x03;
      userLevel|=level&0x03;
      userFlags[target->iId]=userLevel;
      QSqlQuery q(AbexDB);
      q.prepare("INSERT OR REPLACE INTO userFlags (userId, flags) VALUES (?, ?)");
      q.addBindValue(target->iId);
      q.addBindValue(userLevel);
      q.exec();
      server->sendTextMessage(NULL,uSource,false,QString("User %1 is now %2.").arg(target->qsName).arg(isAdmin(target->iId)?"Admin(1)":(isTrusted(target->iId)?"Trusted(2)":"Pleb-Teir(0)")));
    }
  }
  void cipher(Server *server, ServerUser *uSource, QStringList command, QString fullCommand)
  {
    PERM_TRUSTED;
    ServerUser *target = uSource;
    if(command.length()==2){
      target = getUser(server,command[1]);
    }
    if(target==NULL) return server->sendTextMessage(NULL,uSource,false,QString("Bad user!"));
    QSslCipher s = uSource->sessionCipher();
    QString str = (QString("%1<br>%2<br>%3<br>%4<br>%5<br>%6<br>%7")).arg(s.authenticationMethod(),s.encryptionMethod(),s.keyExchangeMethod(),s.name(),s.protocolString()).arg(s.supportedBits()).arg(s.usedBits());
    server->sendTextMessage(NULL,uSource,false,str);
  }
  void help(Server *server, ServerUser *uSource, QStringList command, QString fullCommand)
  {
    server->sendTextMessage(NULL,uSource,false,QString("AbexMod Help<br>"
    "/id [name|id|all|]<br>"
    "/name [new name]|reset [name|id|]<br>"
    "/echo<br>"
    "/emote add|list|clear| [needle] <br>"
    "/emote reload"
    "/perm [user|id] [level|]<br>"
    "/cipher<br>"
    "/version"
    ));
  }
}

void Abex::onConnection(Server *server, ServerUser *user)
{
  originalNames[server->iServerNum][user->iId] = user->qsName;
}
void Abex::onDisconnection(Server *server, ServerUser *user)
{
  originalNames[server->iServerNum].erase(user->iId);
  doEcho[server->iServerNum].erase(user->iId);
}
void Abex::onStateChange(Server *server, ServerUser *user)
{
}

bool Abex::textMessageHandler(Server *server,ServerUser *uSource, MumbleProto::TextMessage &msgPacket)
{
#ifdef textHandlerTryCatch
  //try{
#endif
/*
 *read from packet
 *handle echo request
 *handle enroll request
 *command
 *emote
 */
  string _smsg = msgPacket.message();
  QString msg = QString::fromUtf8(_smsg.data(),_smsg.size());
  if(doEcho[server->iServerNum].count(uSource->iId) && doEcho[server->iServerNum][uSource->iId])
  {
    QString eMsg(msg);
    encode(eMsg);
    server->sendTextMessage(NULL,uSource,false,QString("<pre>%1</pre>").arg(eMsg));
  }
  if(doEnroll[server->iServerNum].count(uSource->iId))
  {
    if(msg=="clear") {
      msg="";
    }
    QString key = doEnroll[server->iServerNum][uSource->iId];
    doEnroll[server->iServerNum].erase(uSource->iId);
    readEmotes(key, msg);
    if (msg == "") {
      server->sendTextMessage(NULL,uSource,false,QString("'%1' is now cleared").arg(key));
    } else {
      server->sendTextMessage(NULL,uSource,false,QString("'%1' is now '%2'").arg(key).arg(msg));
    }
    return true;
  }
  if(msg[0]=='!')
  {
    ServerUser* bot = NULL;
    for(QHash<unsigned int, ServerUser *>::iterator it = server->qhUsers.begin();it != server->qhUsers.end();it++)
    {
				ServerUser *user = it.value();
        if(user->iId>0 && !Abex::showInList(user))
        {
          bot=user;
          break;
        }
    }
    if(bot)
    {
      msg.remove(0,1);
      msgPacket.set_message(u8(msg));
      msgPacket.clear_channel_id();
      msgPacket.clear_session();
      msgPacket.add_session(bot->uiSession);
    }
    return false;
  }
  if(msg[0]!='/')
  {// dont kill the packet
    bool changed=false;
    QString emoteMsg = emoteSearch(msg, &changed);
    msgPacket.set_message(u8(emoteMsg));
    if(changed) // return to client
      server->sendTextMessage(NULL,uSource,false,emoteMsg);
    return false;
  }
  QStringList parts;
  QString currentPart="";
  QChar delimiter='\0';
  decode(msg);
  bool doDelim=false;
  for(int i=0;i<msg.length();i++)
  {
    if(i==0 || msg[i]==' ' && !doDelim)//begining of a substr
    {
      if(msg.length()>i+1) // and there are chars after
      {
        if(currentPart.length()>0) parts.append(currentPart);
        currentPart="";
        if(msg[i+1]=='\'' || msg[i+1]=='\"')
        {
          delimiter=msg[++i];
          doDelim=true;
        }
        else
        {
          delimiter='\0';
          doDelim=false;
        }
      }
    }
    else if(msg[i]=='\\')
    {
      ++i;
      if(msg[i]=='\\')
        currentPart.append('\\');
      else if(msg[i]==delimiter)
        currentPart.append(delimiter);
    }
    else if(msg[i]==delimiter)
      doDelim=!doDelim;
    else
      currentPart.append(msg[i]);
  }
  parts.append(currentPart);

  /*for(int i=0;i<parts.length();i++)
    qWarning() << parts[i];*/

  QString command=parts[0];
  if(command == "id")
    command::id(server,uSource,parts,msg);
  else if(command == "name")
    command::name(server,uSource,parts,msg);
  else if(command == "echo")
    command::echo(server,uSource,parts,msg);
  else if(command == "emote")
    command::emote(server,uSource,parts,msg);
  else if(command == "perm")
    command::perm(server,uSource,parts,msg);
  else if(command == "help")
    command::help(server,uSource,parts,msg);
  else if(command == "cipher")
    command::cipher(server,uSource,parts,msg);
  //else if(command == "chan")
  //  command::chan(server,uSource,parts,msg);
  else if(command == "version")
    server->sendTextMessage(NULL,uSource,false,QString("This server is running Murmur %1 and AbexMod %2").arg(version).arg("0.1.7"));
  else
     server->sendTextMessage(NULL,uSource,false,QString("Unknown Command: '%1'").arg(command));
  return true;
#ifdef textHandlerTryCatch
  //} catch(...){stacktrace(0);return true;}
#endif
}

void stacktrace(int sig)
{
  void *array[10];
  size_t size;

  // get void*'s for all entries on the stack
  size = backtrace(array, 10);

  // print out all the frames to stderr
  qWarning("==Stacktrace==");
  char ** bt = backtrace_symbols(array, size);
  for(int i=0;i<size;i++)
    qWarning(bt[i]);
  //exit(1);
}

//extern QSqlDatabase *ServerDB::db;

#define DB_LOG( VAL ) if(! VAL ) qWarning() << q.lastError()

void Abex::init(QString version_)
{
  version = version_;
  //signal(SIGSEGV,stacktrace);
  readEmotes();
  AbexDB = QSqlDatabase::addDatabase("QSQLITE","AbexDB");
  QFileInfo dbName(ServerDB::db->databaseName());
  AbexDB.setDatabaseName(dbName.path()+"/abex.sqlite");
  if(!AbexDB.open())
  {
    qFatal(QString("Cannot open database at %1").arg(dbName.path()+"/abex.sqlite").toLocal8Bit());
  }
  QSqlQuery q = AbexDB.exec("CREATE TABLE IF NOT EXISTS userFlags(userID INTEGER NOT NULL UNIQUE, flags INTEGER)");
  DB_LOG(q.exec());
  q = AbexDB.exec("SELECT ALL userID, flags FROM userFlags");
  while(q.next())
  {
    userFlags[q.value(0).toInt()] = q.value(1).toInt();
  }
  netman = new QNetworkAccessManager();
  foreach (const QHostAddress &address, QNetworkInterface::allAddresses()) {
      if (address.protocol() == QAbstractSocket::IPv4Protocol && address != QHostAddress(QHostAddress::LocalHost))
           localip = new QHostAddress(address);
  }
  if (localip==NULL) {
    localip = new QHostAddress(QHostAddress::LocalHost);
  }
}
