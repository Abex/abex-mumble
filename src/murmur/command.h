#include "Mumble.pb.h"
#include "ServerUser.h"

namespace Abex
{
  bool textMessageHandler(Server *server,ServerUser *uSource, MumbleProto::TextMessage &msg);
  void onConnection(Server *server,ServerUser *user);
  void onDisconnection(Server* server,ServerUser *user); //TODO Implement
  void onStateChange(Server *server, ServerUser *user);
  void init(QString str);
  bool userShouldSeeChannel(ServerUser *user, Channel *channel);
  bool showInList(ServerUser *user);
}
